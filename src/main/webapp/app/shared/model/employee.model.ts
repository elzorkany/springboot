export interface IEmployee {
    id?: number;
    name?: string;
    email?: string;
    phone?: string;
}

export class Employee implements IEmployee {
    constructor(public id?: number, public name?: string, public email?: string, public phone?: string) {}
}
