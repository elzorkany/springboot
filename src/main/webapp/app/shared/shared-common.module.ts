import { NgModule } from '@angular/core';

import { SpringbootSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [SpringbootSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [SpringbootSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class SpringbootSharedCommonModule {}
