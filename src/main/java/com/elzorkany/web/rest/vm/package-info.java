/**
 * View Models used by Spring MVC REST controllers.
 */
package com.elzorkany.web.rest.vm;
